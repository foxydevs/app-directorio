import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { ModalController, ActionSheetController } from '@ionic/angular';
import { ModalAnuncioPage } from './modal-anuncio/modal-anuncio.page';
import { AnuncioService } from 'src/app/service/anuncio.service';
import { ComercioService } from 'src/app/service/comercio.service';

@Component({
  selector: 'app-anuncio',
  templateUrl: './anuncio.page.html',
  styleUrls: ['./anuncio.page.scss'],
})
export class AnuncioPage implements OnInit {
  private parameter:number;
  private type:number;
  private table:any[] = [];
  private data:any;

  constructor(private activatedRoute: ActivatedRoute,
    private router: Router,
    private location: Location,
    private modalController: ModalController,
    private actionSheetController: ActionSheetController,
    private mainService: AnuncioService,
    private secondService: ComercioService) {
    this.parameter = +this.activatedRoute.snapshot.paramMap.get('id');
    this.type = +this.activatedRoute.snapshot.paramMap.get('type');
    this.getSingle(this.parameter);
  }

  ngOnInit() {
    this.getAll();
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  getAll() {
    this.mainService.getAll()
    .subscribe((res) => {
      this.table = [];
      for(let x of res) {
        if(x.comercio == this.parameter) {
          this.table.push(x);
        }
      }
    }, (error) => {
      console.log(error);
    })
  }

  getSingle(id:number) {
    this.secondService.getSingle(id)
    .subscribe((res) => {
      this.data = res;
    }, (error) => {
      console.log(error);
    })
  }
  
  async presentModal(id?:number) {
    const modal = await this.modalController.create({
      component: ModalAnuncioPage,
      componentProps: { 
        value: id,
        comercio: this.parameter
      }
    });
    modal.onDidDismiss().then((data) => {
      if(data) {
        this.getAll();
      }
    });
    return await modal.present();
  }

    //ACTION SHEET
  async presentActionSheet(id:number) {
    if(this.type == 1) {
      const actionSheet = await this.actionSheetController.create({
        header: 'Opciones',
        buttons: [{
          text: 'Actualizar',
          icon: 'create',
          handler: () => {
            this.presentModal(id);
          }
        }, {
          text: 'Información',
          icon: 'information-circle-outline',
          handler: () => {
            this.goToRoute('home/anuncio-detalle/' + id);
          }
        },
        {
          text: 'Cancelar',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
      });
      await actionSheet.present();
    } else if(this.type == 2) {
      console.log("INFORMACION")
      this.goToRoute('home/anuncio-detalle/' + id);
    }
  }

}
