import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { RecomendacionService } from 'src/app/service/recomendacion.service';

@Component({
  selector: 'app-mis-recomendados',
  templateUrl: './mis-recomendados.page.html',
  styleUrls: ['./mis-recomendados.page.scss'],
})
export class MisRecomendadosPage implements OnInit {
  private table:any[] = [];
  private usuarioID:any = localStorage.getItem('currentId');

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private location:Location,
    private mainService: RecomendacionService) { }

  ngOnInit() {
    this.getAll(this.usuarioID);
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  getAll(id:number) {
    this.mainService.getFilter(id, 'usuario')
    .subscribe((res) => {
      this.table = [];
      this.table = res;
    }, (error) => {
      console.log(error);
    })
  }

}
