import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectorioDetallePage } from './directorio-detalle.page';

describe('DirectorioDetallePage', () => {
  let component: DirectorioDetallePage;
  let fixture: ComponentFixture<DirectorioDetallePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectorioDetallePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectorioDetallePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
