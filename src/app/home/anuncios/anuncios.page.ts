import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AnuncioService } from 'src/app/service/anuncio.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-anuncios',
  templateUrl: './anuncios.page.html',
  styleUrls: ['./anuncios.page.scss'],
})
export class AnunciosPage implements OnInit {
  private parameter:number;
  private type:number;
  private table:any[] = [];
  private search:string;

  constructor(private activatedRoute: ActivatedRoute,
    private router: Router,
    private location: Location,
    private mainService: AnuncioService) {
    
  }

  ngOnInit() {
    this.parameter = +this.activatedRoute.snapshot.paramMap.get('id');
    this.getAll(this.parameter);
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  getAll(id:number) {
    this.mainService.getFilter(id, 'sub_categoria')
    .subscribe((res) => {
      this.table = [];
      this.table = res;
    }, (error) => {
      console.log(error);
    })
  }

}
