import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MisAlianzasPage } from './mis-alianzas.page';

describe('MisAlianzasPage', () => {
  let component: MisAlianzasPage;
  let fixture: ComponentFixture<MisAlianzasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MisAlianzasPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MisAlianzasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
