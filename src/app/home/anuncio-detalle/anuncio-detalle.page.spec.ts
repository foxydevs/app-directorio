import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnuncioDetallePage } from './anuncio-detalle.page';

describe('AnuncioDetallePage', () => {
  let component: AnuncioDetallePage;
  let fixture: ComponentFixture<AnuncioDetallePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnuncioDetallePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnuncioDetallePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
