import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ModalController } from '@ionic/angular';
import { ComercioService } from 'src/app/service/comercio.service';

@Component({
  selector: 'app-directorio',
  templateUrl: './directorio.page.html',
  styleUrls: ['./directorio.page.scss'],
})
export class DirectorioPage implements OnInit {
  private data = {
    fondo: 'https://www.10wallpaper.com/wallpaper/2560x1600/1711/Office_Desk_Keyboard_Art_Cup_Photo_HD_Wallpaper_2560x1600.jpg'
  }
  private table:any = [];
  private search:string;
  
  constructor(private router: Router,
    private location: Location,
    private modalController: ModalController,
    private mainService: ComercioService) { }

  ngOnInit() {
    this.getAll();
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  getAll() {
    this.mainService.getAll()
    .subscribe((res) => {
      console.log(res)
      this.table = [];
      this.table = res;
    }, (error) => {
      console.log(error);
    })
  }

}
