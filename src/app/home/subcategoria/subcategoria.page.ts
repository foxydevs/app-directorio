import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalController, ActionSheetController } from '@ionic/angular';
import { Location } from '@angular/common';
import { SubcategoriaService } from 'src/app/service/subcategoria.service';
import { CategoriaService } from 'src/app/service/categoria.service';
import { ModalSubcategoriaComponent } from './modal-subcategoria/modal-subcategoria.component';

@Component({
  selector: 'app-subcategoria',
  templateUrl: './subcategoria.page.html',
  styleUrls: ['./subcategoria.page.scss'],
})
export class SubcategoriaPage implements OnInit {
  private title:string = 'Subcategorias';
  private table:any[] = [];
  private parameter:number;
  private type:number;
  private data:any;
  private search:string;
  private administratorID = localStorage.getItem('currentId');

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private location:Location,
    private modalController: ModalController,
    private mainService: SubcategoriaService,
    private secondService: CategoriaService,
    private actionSheetController: ActionSheetController) {
      this.parameter = +this.activatedRoute.snapshot.paramMap.get('id');
      this.type = +this.activatedRoute.snapshot.paramMap.get('type');
      this.getSingle(this.parameter)
    }

  ngOnInit() {
    this.getAll();
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  async presentModal(id?:number) {
    const modal = await this.modalController.create({
      component: ModalSubcategoriaComponent,
      componentProps: { 
        value: id,
        category: this.parameter
      }
    });
    modal.onDidDismiss().then((data) => {
      if(data) {
        this.getAll();
      }
    });
    return await modal.present();
  }

  getAll() {
    this.mainService.getAll()
    .subscribe((res) => {
      this.table = [];
      for(let x of res) {
        console.log(x.categoria == this.parameter)
        if(x.categoria == this.parameter) {
          this.table.push(x);
        }
      }
    }, (error) => {
      console.log(error);
    })
  }

  getSingle(id:number) {
    this.secondService.getSingle(id)
    .subscribe((res) => {
      this.data = res;
    }, (error) => {
      console.log(error);
    })
  }

  //ACTION SHEET
  async presentActionSheet(id:number) {
    if(this.administratorID == '1') {
      const actionSheet = await this.actionSheetController.create({
        header: 'Opciones',
        buttons: [{
          text: 'Actualizar',
          icon: 'create',
          handler: () => {
            this.presentModal(id);
          }
        }, {
          text: 'Comercios',
          icon: 'apps',
          handler: () => {
            this.goToRoute('home/comercio/' + id);
          }
        },
        {
          text: 'Anuncios',
          icon: 'globe',
          handler: () => {
            this.goToRoute('home/anuncios/' + id);
          }
        },
        {
          text: 'Cancelar',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
      });
      await actionSheet.present();
    } else {
      if(this.type == 1) {
        this.goToRoute('home/comercio/' + id);
      } else {
        this.goToRoute('home/anuncios/' + id);
      }
    }    
  }

}
