import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { AnuncioService } from 'src/app/service/anuncio.service';
import { NotificacionService } from 'src/app/service/notificacion.service';
import { ComercioService } from 'src/app/service/comercio.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { AlianzaService } from 'src/app/service/alianza.service';
import { RecomendacionService } from 'src/app/service/recomendacion.service';

@Component({
  selector: 'app-anuncio-detalle',
  templateUrl: './anuncio-detalle.page.html',
  styleUrls: ['./anuncio-detalle.page.scss'],
})
export class AnuncioDetallePage implements OnInit {
  private parameter:number;
  private type:number;
  private data:any;
  private commerce:any;
  private selectItem:string;

  constructor(private activatedRoute: ActivatedRoute,
    private router: Router,
    private location: Location,
    private iab: InAppBrowser,
    private mainService: AnuncioService,
    private secondService: ComercioService,
    private thirdService: AlianzaService,
    private fourthService: RecomendacionService,
    private notificationService: NotificacionService,) {
    this.parameter = +this.activatedRoute.snapshot.paramMap.get('id');
    this.getSingle(this.parameter);
  }

  ngOnInit() {
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  segmentChanged(ev: any) {
    this.selectItem = ev.detail.value;
  }

  getSingle(id:number) {
    this.mainService.getSingle(id)
    .subscribe((res) => {
      this.data = res;
      this.getSingleSecond(res.comercio);
    }, (error) => {
      console.error(error);
    })
  }

  getSingleSecond(id:number) {
    this.secondService.getSingle(id)
    .subscribe((res) => {
      this.commerce = res;
      console.log(res)
    }, (error) => {
      console.error(error);
    })
  }

  createThird(alianza: number) {
    let data = {
      comercio: +localStorage.getItem('currentComercio'),
      alianza: alianza,
      state: 0,
      tipo: 0,
    }
    this.thirdService.create(data)
    .subscribe((res) => {
      this.notificationService.alertMessage('Alianza Realizada', 'La alianza se ha realizado con éxito.');
    }, (error) => {
      console.log(data)
      console.error(error)
    })
  }

  createFourth(recomendado: number) {
    let data = {
      usuario: +localStorage.getItem('currentId'),
      recomendado: recomendado,
      state: 0,
      tipo: 0,
    }
    this.fourthService.create(data)
    .subscribe((res) => {
      this.notificationService.alertMessage('Recomendación Agregada', 'La recomendación se ha realizado con éxito.');
    }, (error) => {
      console.log(data)
      console.error(error)
    })
  }

  openBrowser(link:string) {
    const browser = this.iab.create(link, '_self', {toolbarcolor: '#272727'});
  }

}
