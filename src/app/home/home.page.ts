import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ModalController } from '@ionic/angular';
import { ComercioService } from '../service/comercio.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  private data = {
    id: localStorage.getItem('currentId'),
    firstname: localStorage.getItem('currentFirstName'),
    lastname: localStorage.getItem('currentLastName'),
    email: localStorage.getItem('currentEmail'),
    foto: localStorage.getItem('currentPicture'),
    fondo: 'https://www.10wallpaper.com/wallpaper/2560x1600/1711/Office_Desk_Keyboard_Art_Cup_Photo_HD_Wallpaper_2560x1600.jpg'
  }
  private table:any = [];
  
  constructor(private router: Router,
    private location:Location,
    private modalController: ModalController,
    private mainService: ComercioService) { }

  ngOnInit() {
    this.getAll();
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  logOut() {
    localStorage.clear();
    this.goToRoute('login');
  }

  getAll() {
    this.mainService.getAll()
    .subscribe((res) => {
      this.table = [];
      for(let x of res) {
        if(x.usuario == this.data.id) {
          this.table.push(x);
        }
      }
    }, (error) => {
      console.log(error);
    })
  }

  goToRouteGetID(route:string, id:any) {
    this.goToRoute(route);
    localStorage.setItem('currentComercio', id);
  }

  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      this.getAll();
      event.target.complete();
    }, 1000);
  }
}
