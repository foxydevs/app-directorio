import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ModalController, ActionSheetController } from '@ionic/angular';
import { ComercioService } from 'src/app/service/comercio.service';
import { ModalComercioComponent } from './modal-comercio/modal-comercio.component';
import { SubcategoriaService } from 'src/app/service/subcategoria.service';

@Component({
  selector: 'app-comercio',
  templateUrl: './comercio.page.html',
  styleUrls: ['./comercio.page.scss'],
})
export class ComercioPage implements OnInit {
  private data:any;
  private search:string;
  private comercios:any = [];
  private parameter:any;
  private usuarioID:number = +localStorage.getItem('currentId');

  constructor(private router: Router,
    private location:Location,
    private modalController: ModalController,
    private actionSheetController: ActionSheetController,
    private activatedRoute: ActivatedRoute,
    private mainService: ComercioService,
    private secondService: SubcategoriaService) {
      this.parameter = +this.activatedRoute.snapshot.paramMap.get('id');
      this.getSingle(this.parameter)
    }

  ngOnInit() {
    this.getAll();
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  async presentModal(id?:number) {
    const modal = await this.modalController.create({
      component: ModalComercioComponent,
      componentProps: { 
        value: id,
        sub_categoria: this.parameter
      }
    });
    modal.onDidDismiss().then((data) => {
      //DATOS
      if(data) {
        this.getAll();
      }
    });
    return await modal.present();
  }

  getAll() {
    this.mainService.getAll()
    .subscribe((res) => {
      this.comercios = [];
      for(let x of res) {
        if(x.sub_categoria == this.parameter && x.usuario == this.usuarioID) {
          this.comercios.push(x);
        }
      }
    }, (error) => {
      console.log(error);
    })
  }

  //GET SINGLE
  getSingle(id:number) {
    this.secondService.getSingle(id)
    .subscribe((res) => {
      this.data = res;
    }, (error) => {
      console.log(error);
    })
  }

  //ACTION SHEET
  async presentActionSheet(id:number) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Opciones',
      buttons: [{
        text: 'Actualizar',
        icon: 'create',
        handler: () => {
          this.presentModal(id);
        }
      }, {
        text: 'Anuncios',
        icon: 'globe',
        handler: () => {
          this.goToRoute('home/anuncio/1/' + id);
        }
      },
      {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

}
