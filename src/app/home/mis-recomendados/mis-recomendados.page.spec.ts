import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MisRecomendadosPage } from './mis-recomendados.page';

describe('MisRecomendadosPage', () => {
  let component: MisRecomendadosPage;
  let fixture: ComponentFixture<MisRecomendadosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MisRecomendadosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MisRecomendadosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
