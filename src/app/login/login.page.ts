import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { AuthService } from '../service/auth.service';
import { NotificacionService } from '../service/notificacion.service';
import { ModalController } from '@ionic/angular';
import { ModalRegistroComponent } from './modal-registro/modal-registro.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  private disabledBtn:boolean = false;
  private passwordType:string = 'password';
  private passwordShow:boolean = false;
  private data = {
    username: '',
    password: ''
  }

  constructor(private router: Router,
    private location:Location,
    private mainService: AuthService,
    private secondService: NotificacionService,
    private modalController: ModalController) { }

  ngOnInit() {
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  //TOGGLE PASSWORD
  togglePassword() {
    if(this.passwordShow) {
      this.passwordShow = false;
      this.passwordType = 'password';
    } else {
      this.passwordShow = true;
      this.passwordType = 'text';
    }
  }

  authentication() {
    if(this.data.username) {
      if(this.data.password) {
        this.auth();
      } else {
        this.secondService.alertToast("La contraseña es requerida.");
      }
    } else {
      this.secondService.alertToast("El usuario es requerido.");
    }
  }

  auth() {
    this.disabledBtn = true;
    this.mainService.auth(this.data)
    .subscribe((res) => {
      localStorage.setItem('currentUser', res.username);
      localStorage.setItem('currentId', res.id);
      localStorage.setItem('currentFirstName', res.firstname);
      localStorage.setItem('currentLastName', res.lastname);
      localStorage.setItem('currentPicture', res.picture);
      localStorage.setItem('currentEmail', res.email);
      localStorage.setItem('currentState', res.state);
      this.goToRoute('home');
      this.disabledBtn = false;
    }, (error) => {
      console.log(this.data)
      console.error(error);
      this.disabledBtn = false;
      this.secondService.alertToast("Usuario o contraseña incorrectos.");
    });
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ModalRegistroComponent
    });
    modal.onDidDismiss().then((data) => {
      //DATOS
    });
    return await modal.present();
  }

}
