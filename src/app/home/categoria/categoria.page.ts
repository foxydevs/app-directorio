import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { CategoriaService } from 'src/app/service/categoria.service';
import { ComercioService } from 'src/app/service/comercio.service';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.page.html',
  styleUrls: ['./categoria.page.scss'],
})
export class CategoriaPage implements OnInit {
  private search:string = '';
  private categorias:any = [];
  private data:any;

  constructor(private router: Router,
    private location: Location,
    private mainService: CategoriaService,
    private secondService: ComercioService) { }

  ngOnInit() {
    this.getAll();
    this.getSingle(+localStorage.getItem('currentComercio'));
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  getAll() {
    this.mainService.getAll()
    .subscribe((res) => {
      console.log(res)
      this.categorias = res;
    }, (error) => {
      console.log(error);
    })
  }

  //GET SINGLE
  getSingle(id:number) {
    this.secondService.getSingle(id)
    .subscribe((res) => {
      this.data = res;
    }, (error) => {
      console.log(error);
    })
  }
}
