import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { ModalAnuncioPage } from './anuncio/modal-anuncio/modal-anuncio.page';
import { ModalComercioComponent } from './comercio/modal-comercio/modal-comercio.component';
import { ModalCategoriaComponent } from './mi-perfil/modal-categoria/modal-categoria.component';
import { ModalSubcategoriaComponent } from './subcategoria/modal-subcategoria/modal-subcategoria.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      },{
        path: 'directorio-detalle/:id',
        loadChildren: './directorio-detalle/directorio-detalle.module#DirectorioDetallePageModule',
      },{ 
        path: 'mi-perfil/:id', 
        loadChildren: './mi-perfil/mi-perfil.module#MiPerfilPageModule' 
      },{ 
        path: 'subcategoria/:type/:id', 
        loadChildren: './subcategoria/subcategoria.module#SubcategoriaPageModule' },
      { 
        path: 'anuncio-detalle/:id', 
        loadChildren: './anuncio-detalle/anuncio-detalle.module#AnuncioDetallePageModule' 
      },{ 
        path: 'anuncio/:type/:id', 
        loadChildren: './anuncio/anuncio.module#AnuncioPageModule' 
      },{ 
        path: 'comercio/:id', 
        loadChildren: './comercio/comercio.module#ComercioPageModule' 
      },{ 
        path: 'directorio', 
        loadChildren: './directorio/directorio.module#DirectorioPageModule'
      },{ 
        path: 'categoria', 
        loadChildren: './categoria/categoria.module#CategoriaPageModule'
      },{ 
        path: 'anuncios/:id', 
        loadChildren: './anuncios/anuncios.module#AnunciosPageModule' 
      },{
        path: 'mis-alianzas', 
        loadChildren: './mis-alianzas/mis-alianzas.module#MisAlianzasPageModule'
      },{ 
        path: 'mis-recomendados', 
        loadChildren: './mis-recomendados/mis-recomendados.module#MisRecomendadosPageModule'
      },
    ])
  ],
  declarations: [
    HomePage,
    ModalComercioComponent,
    ModalCategoriaComponent,
    ModalSubcategoriaComponent,
    ModalAnuncioPage,
  ],
  entryComponents : [
    ModalComercioComponent,
    ModalCategoriaComponent,
    ModalSubcategoriaComponent,
    ModalAnuncioPage,
  ]
})
export class HomePageModule {}
