import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { AlianzaService } from 'src/app/service/alianza.service';

@Component({
  selector: 'app-mis-alianzas',
  templateUrl: './mis-alianzas.page.html',
  styleUrls: ['./mis-alianzas.page.scss'],
})
export class MisAlianzasPage implements OnInit {
  private table:any[] = [];
  private comercioID:any = localStorage.getItem('currentComercio');

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private location:Location,
    private mainService: AlianzaService) { }

  ngOnInit() {
    this.getAll(this.comercioID);
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  getAll(id:number) {
    this.mainService.getFilter(id, 'comercio')
    .subscribe((res) => {
      this.table = [];
      this.table = res;
    }, (error) => {
      console.log(error);
    })
  }

}
