import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-producto-detalle',
  templateUrl: './producto-detalle.page.html',
  styleUrls: ['./producto-detalle.page.scss'],
})
export class ProductoDetallePage implements OnInit {
  selectItem:any = 'general';

  constructor(private router: Router,
    private location:Location) { }

  ngOnInit() {
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  segmentChanged(ev: any) {
    console.log('Segment changed', ev);
    this.selectItem = ev.detail.value;
    console.log(this.selectItem)
  }

}
