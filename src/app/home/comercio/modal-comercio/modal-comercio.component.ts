import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { path } from 'src/app/config.module';
import { NotificacionService } from 'src/app/service/notificacion.service';
import { ComercioService } from 'src/app/service/comercio.service';
import { SubcategoriaService } from 'src/app/service/subcategoria.service';

//JQUERY
declare var $:any;

@Component({
  selector: 'app-modal-comercio',
  templateUrl: './modal-comercio.component.html',
  styleUrls: ['./modal-comercio.component.scss'],
})
export class ModalComercioComponent implements OnInit {
  private title = 'Formulario Comercio';
  private disabledBtn:boolean = false;
  private disabledColor:boolean = false;
  private basePath:string = path.path;
  private subcategorias:any[] = [];
  private data = {
    id: '',
    nombre: '',
    description: '',
    usuario: localStorage.getItem('currentId'),
    foto: localStorage.getItem('currentPicture'),
    color: '',
    fondo: '',
    link: '',
    state: 0,
    tipo: 0,
    categoria: '',
    sub_categoria: ''
  }
  
  constructor(
    private modalController: ModalController,
    private mainService: ComercioService,
    private secondService: NotificacionService,
    private thirdService: SubcategoriaService,
    private navParams: NavParams
  ) {
    this.data.id = this.navParams.get('value');
    this.getAll();
    if(this.data.id) {
      this.getSingle(this.data.id);
    }
  }

  ngOnInit() {
    
  }

  // MOSTRAR/OCULTAR
  public showAndHide() {
    if(this.disabledColor) {
      this.disabledColor = false;
    } else {
      this.disabledColor = true;
    }
  }

  //GUARDAR CAMBIOS
  public saveChanges() {
    this.data.foto = $('img[alt="Avatar"]').attr('src');
    this.data.categoria = this.returnCategory(+this.data.sub_categoria)
    if(this.data.nombre) {
      this.disabledBtn = true;
      if(this.data.id) {
        this.update();
      } else {
        this.create();
      }
    } else {
      this.secondService.alertToast('El nombre es requerido.');
    }
  }

  //CERRAR MODAL
  closeModal() {
    this.modalController.dismiss();
  }
  
  //OBTENER COLOR
  setColor(color:string) {
    this.data.color = color;
  }

  //ACTUALIZAR
  update() {
    this.mainService.update(this.data)
    .subscribe((res) => {
      this.modalController.dismiss(res);
      this.secondService.alertMessage('Comercio Actualizado', 'El comercio fue actualizado exitosamente.');
      this.disabledBtn = false;
    }, (error) => {
      console.log(this.data)
      console.error(error)
      this.disabledBtn = false;
    })
  }

  //AGREGAR
  create() {
    this.mainService.create(this.data)
    .subscribe((res) => {
      this.modalController.dismiss(res);
      this.secondService.alertMessage('Comercio Agregado', 'El comercio fue agregado exitosamente.');
      this.disabledBtn = false;
    }, (error) => {
      console.log(this.data)
      console.error(error)
      this.disabledBtn = false;
    })
  }

  //GET SINGLE
  getSingle(id: any) {
    this.mainService.getSingle(id)
    .subscribe((res) => {
      console.log(res)
      this.secondService.alertLoading('Cargando...', 5000);
      this.secondService.dismiss();
      this.data = res;
    }, (error) => {
      console.error(error);
      this.secondService.dismiss();
    })
  }

  //GET ALL
  getAll() {
    this.thirdService.getAll()
    .subscribe((res) => {
      this.subcategorias = res;
      if(!this.data.id) {
        this.data.sub_categoria = this.navParams.get('sub_categoria');
      }
    }, (error) => {
      console.log(error);
    })
  }

  //RETURN CATEGORIA
  returnCategory(id:number) {
    for(let x of this.subcategorias) {
      if(x.id == id) {
        return x.categoria;
      }
    }
    return '';
  }

  //CAMBIAR FOTO DE PERFIL
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}upload`;
    var size=archivos[0].size;
    var type=archivos[0].type;
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://i.gifer.com/H8An.gif')
          $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'Comercio'
          },
          function(res)
          {
            console.log(res)
            $('#imgAvatar').attr("src", res)
            $("#"+id).val('')
        });
      } else {
        this.secondService.alertToast('La imagen es demasiado grande.')
      }
    } else {
      this.secondService.alertToast('El tipo de imagen no es válido.');
    }
  }

}
