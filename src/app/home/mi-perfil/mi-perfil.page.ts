import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ModalController, ActionSheetController } from '@ionic/angular';
import { CategoriaService } from 'src/app/service/categoria.service';
import { ModalCategoriaComponent } from './modal-categoria/modal-categoria.component';

@Component({
  selector: 'app-mi-perfil',
  templateUrl: './mi-perfil.page.html',
  styleUrls: ['./mi-perfil.page.scss'],
})
export class MiPerfilPage implements OnInit {
  private data = {
    id: localStorage.getItem('currentId'),
    firstname: localStorage.getItem('currentFirstName'),
    lastname: localStorage.getItem('currentLastName'),
    email: localStorage.getItem('currentEmail'),
    foto: localStorage.getItem('currentPicture'),
  }
  private search:string = '';
  private selectItem:any = 'categorias';
  private comercios:any = [];
  private categorias:any = [];

  constructor(private router: Router,
    private location:Location,
    private modalController: ModalController,
    private mainService: CategoriaService,
    private actionSheetController: ActionSheetController) { }

  ngOnInit() {
    this.getAll();
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  segmentChanged(ev: any) {
    this.selectItem = ev.detail.value;
  }

  async presentModalSecond(id?:number) {
    const modal = await this.modalController.create({
      component: ModalCategoriaComponent,
      componentProps: { value: id }
    });
    modal.onDidDismiss().then((data) => {
      //DATOS
      if(data) {
        this.getAll();
      }
    });
    return await modal.present();
  }

  getAll() {
    this.mainService.getAll()
    .subscribe((res) => {
      console.log(res)
      this.categorias = res;
    }, (error) => {
      console.log(error);
    })
  }

  //ACTION SHEET
  async presentActionSheet(id:number) {
    if(this.data.id == '1') {
      const actionSheet = await this.actionSheetController.create({
        header: 'Opciones',
        buttons: [{
          text: 'Actualizar',
          icon: 'create',
          handler: () => {
            this.presentModalSecond(id);
          }
        }, {
          text: 'Subcategoría',
          icon: 'apps',
          handler: () => {
            this.goToRoute('home/subcategoria/1/' + id);
          }
        },
        {
          text: 'Cancel',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
      });
      await actionSheet.present();
    } else {
      this.goToRoute('home/subcategoria/1/' + id);
    }
  }

}
