import { Component, OnInit } from '@angular/core';
import { path } from 'src/app/config.module';
import { ModalController, NavParams } from '@ionic/angular';
import { AnuncioService } from 'src/app/service/anuncio.service';
import { NotificacionService } from 'src/app/service/notificacion.service';

//JQUERY
declare var $:any;

@Component({
  selector: 'app-modal-anuncio',
  templateUrl: './modal-anuncio.page.html',
  styleUrls: ['./modal-anuncio.page.scss'],
})
export class ModalAnuncioPage implements OnInit {
  private title = 'Agregar Anuncio';
  private disabledBtn:boolean = false;
  private disabledColor:boolean = false;
  private basePath:string = path.path;
  private data = {
    id: '',
    nombre: '',
    description: '',
    usuario: localStorage.getItem('currentId'),
    portada: localStorage.getItem('currentPicture'),
    foto1: localStorage.getItem('currentPicture'),
    foto2: localStorage.getItem('currentPicture'),
    foto3: localStorage.getItem('currentPicture'),
    color: '',
    fondo: '',
    link: '',
    state: 0,
    tipo: 0,
    comercio: ''
  }
  slideOpts = {
    effect: 'flip',
    pagination: false
  };

  constructor(
    private modalController: ModalController,
    private mainService: AnuncioService,
    private secondService: NotificacionService,
    private navParams: NavParams,
  ) {
    this.data.id = this.navParams.get('value');
    this.data.comercio = this.navParams.get('comercio');
    if(this.data.id) {
      this.getSingle(this.data.id);
    }
  }

  ngOnInit() {}

  // MOSTRAR/OCULTAR
  public showAndHide() {
    if(this.disabledColor) {
      this.disabledColor = false;
    } else {
      this.disabledColor = true;
    }
  }

  //GUARDAR CAMBIOS
  public saveChanges() {
    this.data.portada = $('img[alt="Avatar"]').attr('src');
    this.data.foto1 = $('img[alt="Avatar1"]').attr('src');
    this.data.foto2 = $('img[alt="Avatar2"]').attr('src');
    this.data.foto3 = $('img[alt="Avatar3"]').attr('src');
    if(this.data.nombre) {
      this.disabledBtn = true;
      if(this.data.id) {
        this.update();
      } else {
        this.create();
      }
    } else {
      this.secondService.alertToast('El nombre es requerido.');
    }
  }

  //CERRAR MODAL
  closeModal() {
    this.modalController.dismiss();
  }
  
  //OBTENER COLOR
  setColor(color:string) {
    this.data.color = color;
  }

  //ACTUALIZAR
  update() {
    this.mainService.update(this.data)
    .subscribe((res) => {
      this.modalController.dismiss(res);
      this.secondService.alertMessage('Anuncio Actualizado', 'El anuncio fue actualizado exitosamente.');
      this.disabledBtn = false;
    }, (error) => {
      console.log(this.data)
      console.error(error)
      this.disabledBtn = false;
    })
  }

  //AGREGAR
  create() {
    this.mainService.create(this.data)
    .subscribe((res) => {
      this.modalController.dismiss(res);
      this.secondService.alertMessage('Anuncio Agregado', 'El anuncio fue agregada exitosamente.');
      this.disabledBtn = false;
    }, (error) => {
      console.log(this.data)
      console.error(error)
      this.disabledBtn = false;
    })
  }

  //GET SINGLE
  getSingle(id: any) {
    this.mainService.getSingle(id)
    .subscribe((res) => {
      this.secondService.alertLoading('Cargando...', 5000);
      this.secondService.dismiss();
      this.data = res;
    }, (error) => {
      console.error(error);
      this.secondService.dismiss();
    })
  }

  //CAMBIAR FOTO DE PERFIL
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}upload`;
    var size=archivos[0].size;
    var type=archivos[0].type;
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://i.gifer.com/H8An.gif')
          $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'Comercio'
          },
          function(res)
          {
            console.log(res)
            $('#imgAvatar').attr("src", res)
            $("#"+id).val('')
        });
      } else {
        this.secondService.alertToast('La imagen es demasiado grande.')
      }
    } else {
      this.secondService.alertToast('El tipo de imagen no es válido.');
    }
  }

  //CAMBIAR FOTO DE PERFIL
  uploadImage1(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}upload`;
    var size=archivos[0].size;
    var type=archivos[0].type;
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar1').attr("src",'https://i.gifer.com/H8An.gif')
          $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'Comercio'
          },
          function(res)
          {
            console.log(res)
            $('#imgAvatar1').attr("src", res)
            $("#"+id).val('')
        });
      } else {
        this.secondService.alertToast('La imagen es demasiado grande.')
      }
    } else {
      this.secondService.alertToast('El tipo de imagen no es válido.');
    }
  }

  //CAMBIAR FOTO DE PERFIL
  uploadImage2(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}upload`;
    var size=archivos[0].size;
    var type=archivos[0].type;
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar2').attr("src",'https://i.gifer.com/H8An.gif')
          $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'Comercio'
          },
          function(res)
          {
            console.log(res)
            $('#imgAvatar2').attr("src", res)
            $("#"+id).val('')
        });
      } else {
        this.secondService.alertToast('La imagen es demasiado grande.')
      }
    } else {
      this.secondService.alertToast('El tipo de imagen no es válido.');
    }
  }

  //CAMBIAR FOTO DE PERFIL
  uploadImage3(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}upload`;
    var size=archivos[0].size;
    var type=archivos[0].type;
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar3').attr("src",'https://i.gifer.com/H8An.gif')
          $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'Comercio'
          },
          function(res)
          {
            console.log(res)
            $('#imgAvatar3').attr("src", res)
            $("#"+id).val('')
        });
      } else {
        this.secondService.alertToast('La imagen es demasiado grande.')
      }
    } else {
      this.secondService.alertToast('El tipo de imagen no es válido.');
    }
  }
}
