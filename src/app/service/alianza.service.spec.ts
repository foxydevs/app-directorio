import { TestBed } from '@angular/core/testing';

import { AlianzaService } from './alianza.service';

describe('AlianzaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AlianzaService = TestBed.get(AlianzaService);
    expect(service).toBeTruthy();
  });
});
