import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ModalController, ActionSheetController } from '@ionic/angular';
import { CategoriaService } from 'src/app/service/categoria.service';
import { ModalCategoriaComponent } from '../mi-perfil/modal-categoria/modal-categoria.component';

@Component({
  selector: 'app-directorio-detalle',
  templateUrl: './directorio-detalle.page.html',
  styleUrls: ['./directorio-detalle.page.scss'],
})
export class DirectorioDetallePage implements OnInit {
  private selectItem:any = 'general';
  private table:any[] = [];

  constructor(private router: Router,
    private location:Location,
    private modalController: ModalController,
    private actionSheetController: ActionSheetController,
    private mainService: CategoriaService) { }

  ngOnInit() {
    this.getAll();
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  //ACTION SHEET
  async presentActionSheet(id:number) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Opciones',
      buttons: [{
        text: 'Actualizar',
        icon: 'create',
        handler: () => {
          console.log('Delete clicked');
          this.presentModal(id);
        }
      }, {
        text: 'Subcategoría',
        icon: 'apps',
        handler: () => {
          console.log('Share clicked');
          this.goToRoute('home/categoria-detalle/' + id);
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  segmentChanged(ev: any) {
    console.log('Segment changed', ev);
    this.selectItem = ev.detail.value;
    console.log(this.selectItem)
  }

  async presentModal(id?:number) {
    const modal = await this.modalController.create({
      component: ModalCategoriaComponent,
      componentProps: { value: id }
    });
    modal.onDidDismiss().then((data) => {
      //DATOS
      if(data) {
        this.getAll();
      }
    });
    return await modal.present();
  }

  getAll() {
    this.mainService.getAll()
    .subscribe((res) => {
      console.log(res)
      this.table = res;
    }, (error) => {
      console.log(error);
    })
  }

}
